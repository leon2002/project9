#include "BSNode.h"
#include <iostream>
#include <algorithm> 

BSNode::BSNode(std::string data)
{
	this->_data = data;
	this->_left = NULL;
	this->_right = NULL;
	this->_count = 1;
}

BSNode::BSNode(const BSNode& other)
{
	//deep recursive copy of other tree
	this->_count = 1;
	this->_data = other._data;
	this->_left = 0;
	this->_right = 0;

	if (other._left){
		this->_left = new BSNode(*other._left);
	}
	if (other._right){
		this->_right = new BSNode(*other._right);
	}

}

BSNode::~BSNode()
{
	if (this != NULL) {
		if ((this->_left == NULL) && (this->_right == NULL)) {
			delete[] this;
		}
		else if (this->_left == NULL)
		{
			this->_right->~BSNode();
			delete[] this;
		}
		else if (this->_right == NULL)
		{
			this->_left->~BSNode();
			delete[] this;
		}
		else
		{
			this->_left->~BSNode();
			this->_right->~BSNode();
			delete[] this;
		}
	}
}

void BSNode::insert(std::string value)
{
	if (value == this->_data) // If the string already exists in BST, count+1 and return 
	{
		this->_count++;
	}
	else if (value < this->_data) {
		if (this->_left != NULL) {
			this->_left->insert(value);
		}
		else {
			this->_left = new BSNode(value);
		}
	}
	else if (value > this->_data) {
		if (this->_right != NULL) {
			this->_right->insert(value);
		}
		else {
			this->_right = new BSNode(value);
		}
	}
}

BSNode& BSNode::operator=(const BSNode& other)
{
	// TODO: insert return statement here
	_data = other._data;
	_left = 0;
	_right = 0;

	//deep recursive copy
	if (other._left)
	{
		_left = new BSNode(*other._left);
	}

	if (other._right)
	{
		_right = new BSNode(*other._right);
	}

	return *this;
}

bool BSNode::isLeaf() const
{
	if (this->_left == NULL && this->_right == NULL) {
		return true;
	}
	return false;
}

std::string BSNode::getData() const
{
	return this->_data;
}

BSNode* BSNode::getLeft() const
{
	return this->_left;
}

BSNode* BSNode::getRight() const
{
	return this->_right;
}

bool BSNode::search(std::string val) const{

	if (this->_data == val){
		return true;
	}
	else if (this->_data > val)
	{
		if (this->_right == NULL){
			search(val);
		}
		search(val);
	}
	else // (this->_data < val)
	{
		if (this->_right == NULL){
			search(val);
		}
		search(val);
	}

	return false;
}

int BSNode::getHeight() const
{
	return  ( 1 + std::max(this->_left == NULL ? 0 : this->_left->getHeight(), this->_right == NULL ? 0 : this->_right->getHeight()));
}

int BSNode::getDepth(const BSNode& root) const
{
	int depth = 0;

	if (this == NULL) {
		return -1;
	}
	else {
		if (this->_data > root.getData()) {
			depth = 1 + this->getDepth(*root.getRight());
		}
		else if (this->_data < root.getData()){
			depth = 1 + this->getDepth(*root.getLeft());
		}

	}
	return depth;
}

void BSNode::printNodes() const
{
	if (this->_left != NULL)
	{
		this->_left->printNodes();
	}
	std::cout << this->_data << std::endl;
	if (this->_right != NULL)
	{
		this->_right->printNodes();
	}
}

int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	return 0;
}