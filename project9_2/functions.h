#pragma once
#include <iostream>


template <class T>
int compare(T first, T second)
{
	if (first < second) {
		return 1;
	}
	else if (first == second) {
		return 0;
	}
	else {
		return -1;
	}
}


template <class T>
void bubbleSort(T* arr, int size)
{
	int i, j;
	T temp;
	bool flag;
	for (i = 0; i < size - 1; i++)
	{
		flag = false;
		for (j = 0; j < size - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;

				flag = true;
			}
		}

		// IF no two elements were swapped by inner loop, then break 
		if (!flag)
			break;
	}
}

template <class T>
void printArray(T* arr, int size)
{
	for (int i = 0; i < size; i++) {
		std::cout << arr[i] << std::endl;
	}
}